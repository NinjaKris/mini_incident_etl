### Mini_Incident_ETL

####Assumptions:
* Ids, discovered date, and status must not be null
* Name and Description may be null
* Given a submission with the same discovered date, status will be used to sort next, then name.

####Requirements:
- [ ] Read JSON input in the format specified below, and output its data in CSV format.
- [ ] Sort the incidents by discovered date, from least recent to most recent.
- [ ] Sort the incidents by status, in the order specified below.
- [ ] Accept a command-line argument sortfield, specifying the field to sort on.
- [ ] Accept a command-line argument sortdirection, specifying the direction of the sort, which recognizes parameter values ascending and descending.
- [ ] Accept a command-line argument columns specifying which columns to output in CSV, accepting a list of field names like id,name, which will restrict the output of the CSV to only the columns specified.

####Test Cases:
TBD

####Notes:
* For Development I am using GoLand and the Go SDK (1.13.1)


